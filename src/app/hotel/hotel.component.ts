import { Component, OnInit } from '@angular/core';
import {UserService} from '../service/user.service';
import {User} from '../data/user';
import {RoomService} from '../service/room.service';
import {Room} from '../data/room';
import {getType} from '@angular/core/src/errors';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {Reserved} from '../data/reserved';
import {ReserveService} from '../service/reserve.service';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {
  constructor(private userService: UserService, private roomService: RoomService, private router: Router,
              private reserveService: ReserveService) { }
  logged_user: User;
  room_id: number;
  rooms: Room[];
  age_ranges: string[] = ['Child', 'Adult' ];
  selected_room: Room;
  selected_room_array: number[];
  reserveds: Reserved[] = [];

  ngOnInit() {
    this.logged_user = this.userService.logged_user;
    this.rooms = this.roomService.getRooms();
    if (!this.userService.logged_user) {
        this.router.navigate(['/user']);
    }
  }
  onRoomChange() {
    if ( this.room_id > 0 ) {
      this.selected_room = this.roomService.selected_room = this.rooms.find((room) => (room.id === Number(this.room_id)));
      this.selected_room_array = new Array(this.selected_room.capacity);
    }
  }
  onSubmitReserve(form: NgForm) {
    const data = form.value;
    for (let i = 0 ; i < this.selected_room.capacity; i++) {
      const reserved: Reserved = { id: i + 1,
          name: data['passenger_name_' + String(i)],
          user_id: this.logged_user.id,
          room_id: this.room_id,
          age_range: data['age_range_' + String(i)],
          age: data['age_' + String(i)]
      };
      this.reserveService.setReserved(reserved);
    }
      this.router.navigate(['/result']);
  }
}
