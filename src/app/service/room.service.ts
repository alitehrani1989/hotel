import { Injectable } from '@angular/core';
import {ROOMS} from '../data/rooms';
import {Room} from '../data/room';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

@Injectable()
export class RoomService {

    Rooms = ROOMS;
    selected_room: Room;
    constructor() { }
    getRooms(): Room[] {
        return this.Rooms;
    }
    setRoom(room: Room): void {
        this.Rooms.push(room);
    }
    getRoom(id: number): Observable<Room> {
        return of(this.Rooms.find(room => room.id === id));
    }
}
