import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {Reserved} from '../data/reserved';
import {Room} from '../data/room';

export class ReserveService {

    Reserveds: Reserved[] = [];
    constructor() { }
    getReserveds(): Reserved[] {
        return this.Reserveds;
    }
    setReserved(reserved: Reserved): void {
        this.Reserveds.push(reserved);
    }
}
