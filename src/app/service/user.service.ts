import { Injectable } from '@angular/core';
import {User} from '../data/user';
import {USERS} from '../data/users';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
@Injectable()
export class UserService {
  Users = USERS;
  logged_user: User;
  constructor() { }
  getUsers(): Observable<User[]>  {
    return of(this.Users);
  }
  setUser(user: User): void {
    this.Users.push(user);
  }
  getUser(id: number): Observable<User> {
        return of(this.Users.find(user => user.id === id));
  }
  setLoggedUser(user: User): void {
      this.logged_user = user;
  }
}
