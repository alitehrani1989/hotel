import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HotelComponent} from './hotel/hotel.component';
import {UserComponent} from './user/user.component';
import {ResultComponent} from './result/result.component';

const routes: Routes = [
    {path: '', redirectTo: '/user', pathMatch: 'full'},
    {path: 'user', component: UserComponent},
    {path: 'result', component: ResultComponent},
    {path: 'hotel', component: HotelComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
