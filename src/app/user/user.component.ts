import { Component, OnInit } from '@angular/core';
import {UserService} from '../service/user.service';
import {User} from '../data/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }
  users: User[];
  error: string;
  username: string;
  password: string;
  ngOnInit() {
    console.log(this.userService.logged_user);
    this.getUsers();
  }
    getUsers(): void {
        this.userService.getUsers()
            .subscribe(users => this.users = users);
    }
    login(): void {
    this.error = null;
      this.userService.logged_user = this.users.find( user => (user.name === this.username && user.password === this.password) );
      if ( this.userService.logged_user ) {
          this.router.navigate(['/hotel']);
      } else {
        this.error = 'User name or passwor is incorrect';
      }
    }
}
