import { Room } from './room';

export const ROOMS: Room[] = [
  { id: 1, name: 'Room One', capacity: 1 },
  { id: 2, name: 'Room Two', capacity: 2 },
  { id: 3, name: 'Room Three', capacity: 3 },
  { id: 4, name: 'Room Four', capacity: 4 },
  { id: 5, name: 'Room Five', capacity: 5 },
  { id: 6, name: 'Room Six', capacity: 6 },

];
