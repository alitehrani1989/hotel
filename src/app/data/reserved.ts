export class Reserved {
  id: number;
  name: string;
  user_id: number;
  room_id:  number;
  age_range: string;
  age: number;
}
