import {Component, OnInit} from '@angular/core';
import {UserService} from '../service/user.service';
import {RoomService} from '../service/room.service';
import {Router} from '@angular/router';
import {ReserveService} from '../service/reserve.service';
import {User} from '../data/user';
import {Room} from '../data/room';

@Component({
    selector: 'app-result',
    templateUrl: './result.component.html',
    styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

    constructor(private userService: UserService, private roomService: RoomService, private router: Router,
                private reserveService: ReserveService) {
    }

    result: Object[];
    user: User;
    room: Room;

    ngOnInit() {
        if (!this.userService.logged_user) {
            this.router.navigate(['/user']);
        }
        this.result = this.reserveService.getReserveds();
        this.user = this.userService.logged_user;
        this.room = this.roomService.selected_room;
    }

}
