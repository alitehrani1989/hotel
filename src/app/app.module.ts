import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms'; // <-- NgModel lives here also add into imports below
import { AppRoutingModule } from './/app-routing.module';
import { UserComponent } from './user/user.component';
import {UserService} from './service/user.service';
import {RoomService} from './service/room.service';
import { HotelComponent } from './hotel/hotel.component';
import { ResultComponent } from './result/result.component';
import {ReserveService} from './service/reserve.service';



@NgModule({
  declarations: [
    AppComponent,
    UserComponent, HotelComponent, ResultComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
      NgbModule.forRoot(),
    AppRoutingModule
  ],
  providers: [ UserService, RoomService, ReserveService],
  bootstrap: [AppComponent]
})
export class AppModule { }
